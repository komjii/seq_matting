"""
Inference on webcams: Use a model on webcam input.

Once launched, the script is in background collection mode.
Press B to toggle between background capture mode and matting mode. The frame shown when B is pressed is used as background for matting.
Press Q to exit.

Example:

    python inference_webcam.py \
        --model-type mattingrefine \
        --model-backbone resnet50 \
        --model-checkpoint "PATH_TO_CHECKPOINT" \
        --resolution 1280 720

"""

import argparse, os, shutil, time
import cv2
import torch

from torch import nn
from torch.utils.data import DataLoader
from torchvision.transforms import Compose, ToTensor, Resize
from torchvision.transforms.functional import to_pil_image
from threading import Thread, Lock
from tqdm import tqdm
from PIL import Image

from dataset import VideoDataset
from model import MattingBase, MattingRefine


# --------------- Arguments ---------------


parser = argparse.ArgumentParser(description='Inference from web-cam')

parser.add_argument('--model-type', type=str, required=True, choices=['mattingbase', 'mattingrefine'])
parser.add_argument('--model-backbone', type=str, required=True, choices=['resnet101', 'resnet50', 'mobilenetv2'])
parser.add_argument('--model-backbone-scale', type=float, default=0.25)
parser.add_argument('--model-checkpoint', type=str, required=True)
parser.add_argument('--model-refine-mode', type=str, default='sampling', choices=['full', 'sampling', 'thresholding'])
parser.add_argument('--model-refine-sample-pixels', type=int, default=80000)
parser.add_argument('--model-refine-threshold', type=float, default=0.7)
parser.add_argument('--ip',      type=str, required=True)
parser.add_argument('--bp', type=str, required=True)
parser.add_argument('--mp',     type=str, required=True)
#parser.add_argument('--op_rq',     type=bool, default = False)
parser.add_argument('--hide-fps', action='store_true')
parser.add_argument('--resolution', type=int, nargs=2, metavar=('width', 'height'), default=(1280, 720))
args = parser.parse_args()


# ----------- Utility classes -------------


# A wrapper that reads data from cv2.VideoCapture in its own thread to optimize.
# Use .read() in a tight loop to get the newest frame


# An FPS tracker that computes exponentialy moving average FPS


# Wrapper for playing a stream with cv2.imshow(). It can accept an image and return keypress info for basic interactivity.
# It also tracks FPS and optionally overlays info onto the stream.


# --------------- Main ---------------



# Load model
if args.model_type == 'mattingbase':
    model = MattingBase(args.model_backbone)
if args.model_type == 'mattingrefine':
    model = MattingRefine(
        args.model_backbone,
        args.model_backbone_scale,
        args.model_refine_mode,
        args.model_refine_sample_pixels,
        args.model_refine_threshold)

model = model.cuda().eval()
model.load_state_dict(torch.load(args.model_checkpoint), strict=False)

def cv2_frame_to_cuda(frame):
    #frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    return ToTensor()(Image.fromarray(frame)).unsqueeze_(0).cuda()
    #return ToTensor()(frame).unsqueeze_(0).cuda()
with torch.no_grad():
    ip_path = os.path.join(os.getcwd(),args.ip)
    folders = os.listdir(ip_path)
    print(folders)
    mp_path = os.path.join(os.getcwd(),args.mp)
    for folder in folders:
        mp_folder_path = os.path.join(mp_path,folder)
        if not os.path.isdir(mp_path):
            os.mkdir(mp_path)
            if not os.path.isdir(mp_folder_path):
                os.mkdir(mp_folder_path)
        else:
            if not os.path.isdir(mp_folder_path):
                os.mkdir(mp_folder_path)
        ip_folder_path = os.path.join(os.getcwd(),args.ip,folder)
        files = os.listdir(ip_folder_path)
        files = [file for file in files if file.endswith(".png") or file.endswith(".jpg")]
        bgr_path = os.path.join(os.getcwd(), args.bp, folder)
        bgr_file = os.listdir(bgr_path)
        bgr_file = bgr_file[0]
        st = time.time()
        bgr = cv2_frame_to_cuda(cv2.imread(os.path.join(bgr_path,bgr_file)))
        #bgr = cv2_frame_to_cuda(Image.open(os.path.join(bgr_path,bgr_file)))
        for file in files:
            src_to_cuda_t = time.time()
            src = cv2_frame_to_cuda(cv2.imread(os.path.join(ip_folder_path,file)))
            #src = cv2_frame_to_cuda(Image.open(os.path.join(ip_folder_path,file)))
            print("src_to_cuda_t : "+str(time.time()-src_to_cuda_t))
            i_st = time.time()
            pha, fgr = model(src, bgr)[:2]
            print("inf time : "+str(time.time()-i_st))
            mk_matte_t = time.time()
            matte = pha.mul(255).byte().cpu().permute(0, 2, 3, 1).numpy()[0]
            print("mk_matte_t : "+str(time.time()-mk_matte_t))
            
            '''
            if args.op_rq:
                if not os.path.isdir(os.path.join(os.getcwd(),"overlay")):
                    os.mkdir(os.path.join(os.getcwd(),"overlay"))
                    if not os.path.isdir(os.path.join(os.getcwd(),"overlay",folder)):
                        os.mkdir(os.path.join(os.getcwd(),"overlay",folder))
                else:
                    if not os.path.isdir(os.path.join(os.getcwd(),"overlay",folder)):
                        os.mkdir(os.path.join(os.getcwd(),"overlay",folder))
                    #os.mkdir(os.path.join(os.getcwd(),"overlay",folder))
                res = pha * fgr + (1 - pha) * torch.ones_like(fgr)
                res = res.mul(255).byte().cpu().permute(0, 2, 3, 1).numpy()[0]
                res = cv2.cvtColor(res, cv2.COLOR_RGB2BGR)
                cv2.imwrite(os.path.join(os.getcwd(),"overlay",folder,file),res)
            '''
            im_write_t = time.time()
            cv2.imwrite(os.path.join(os.getcwd(),args.mp,folder,file),matte)
            print("im_write_t : "+str(time.time()-im_write_t))
    print("elapsed time : "+str(time.time()-st))
    