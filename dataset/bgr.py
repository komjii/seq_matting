import os
import glob
from torch.utils.data import Dataset
from PIL import Image
import cv2

class BgrDataset(Dataset):
    def __init__(self, root, len_files, mode='RGB', transforms=None):
        self.transforms = transforms
        self.mode = mode
        self.cnt = -1
        self.filenames = sorted([*glob.glob(os.path.join(root, '**', '*.jpg'), recursive=True),
                                 *glob.glob(os.path.join(root, '**', '*.png'), recursive=True)])
        self.tmp = ""
        self.size = int(len_files/len(self.filenames))
        
        self.total_len = len_files
        self.img_tmp = None
        self.filenames = sorted(self.filenames * self.size)
    def __len__(self):
        return len(self.filenames)
    '''
    def __getitem__(self, idx):
        if int(idx/self.size) > self.cnt:
            self.cnt += 1
            with Image.open(self.filenames[idx]) as img:
                img = img.convert(self.mode)
                if self.transforms:
                    img = self.transforms(img)
                self.img = img.copy()
        return self.img
    '''
    def __getitem__(self, idx):
        if not self.filenames[idx] == self.tmp:
            with Image.open(self.filenames[idx]) as img:
                img = img.convert(self.mode)
            
            if self.transforms:
                img = self.transforms(img)
            self.img_tmp = img.copy()
            self.tmp = self.filenames[idx]
            #print("is img")
            return img#, self.filenames[idx]
        else:
            #print("skip img")
            return self.img_tmp#, self.filenames[idx]
